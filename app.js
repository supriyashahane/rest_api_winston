var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
const logger = require('./app/logger/logger')(__filename);
app.use(express.static('resources'));
var cors = require('cors');
app.use(cors());
global.__basedir = __dirname;

// Configuring the database
const dbConfig = require('./app/config/mongodb.config.js');
const mongoose = require('mongoose');
 
mongoose.Promise = global.Promise;
 
// Connecting to the database
mongoose.connect(dbConfig.url, { useNewUrlParser: true })
.then(() => {
    logger.info('Server running');
    //console.log("Successfully connected to MongoDB.");
    logger.info("Successfully connected to MongoDB.");    
}).catch(err => {
    console.log('Could not connect to MongoDB.');
    //logger.info('Could not connect to MongoDB');/
    process.exit();
});
 
require('./app/routes/user.route.js')(app);
 
// Create a Server
var server = app.listen(8081, function () {
 
  var host = server.address().address
  var port = server.address().port
 
  //console.log("App listening at http://%s:%s", host, port)
  logger.info("App listening at http://127.0.0.1:8081");
 
})