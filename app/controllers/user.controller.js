const User = require('../models/user.model.js');
const User1 = require('../models/intro.model.js');
const logger = require('../logger/logger.js')(__filename);
const jwt = require('jsonwebtoken');

// Save FormData - User to MongoDB
exports.saveUser = (req, res) => {
    //console.log('Post a User: ' + JSON.stringify(req.body));
    
    // Save a Customer in the MongoDB
    const user = new User({
        message: req.body.message,
        data: req.body.data
        });
    user.save()
    .then(data => {
        logger.info(user);
        logger.info("'post' a user req :", { firstname: user.firstname});  
        logger.info(req.body.data);
        logger.info(req.body.data.token);
        logger.info(req.body.message);
        res.send(req.body.data);
    }).catch(err => {
        logger.info("Request failed returning err", err);
        res.status(500).send({
            message: err.message
        });
    });
};

//save introPage data

exports.introSave = (req, res) => {
    const user = new User1({
        message: req.body.message,
        data : req.body.data
    });
    user.save()
    .then(data => {
        logger.info("'post' a user req :");  
        logger.info(user.data);
        logger.info(req.body.data);
        res.send(req.body.data);
    }).catch(err => {
        logger.info("Request failed returning err", err);
        res.status(500).send({
            message: err.message
        });
    });
};
 
// Fetch all Users
exports.findUsers = (req, res) =>  {
	//console.log("Fetch all Users");
    User.find()
    .then(users => {
        res.send(users);
        logger.info("'get' users request :", { body: users});
        logger.info(users);
    }).catch(err => {
        logger.info("Request failed returning err", err);
        res.status(500).send({
            message: err.message
        });
    });
    //logger.info('/ response', res);
};

//fetch users
exports.findUsers1 = (req, res) =>  {
	//console.log("Fetch all Users");
    User1.find()
    .then(users => {
        res.send(users);
        logger.info("'get' users request :", { body: users});
        logger.info(users);
    }).catch(err => {
        logger.info("Request failed returning err", err);
        res.status(500).send({
            message: err.message
        });
    });
    //logger.info('/ response', res);
};


  
