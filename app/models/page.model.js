const mongoose = require('mongoose');

const PageLink = mongoose.Schema({
    message: String,
    data: {
    linkedPages: {
        id : String,
        name : String
    },
    selectedPages: {
        id : String,
        name : String
    }
}});

module.exports = mongoose.model('page', PageLink, 'users');
