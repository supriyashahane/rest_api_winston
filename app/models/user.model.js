const mongoose = require('mongoose');


const UserSchema = mongoose.Schema({
    message: String,
    
    data: {
        token:String,
        role: String,
        isAccountActive: Boolean,
        isIntroComplete: Boolean,
        fbPagesLinked: Boolean,
        email: {
            type: String,
        },
       
        name: String,
        birthday: String,
        fbId: String,
        picUrl: {
            hight:String,
            is_silhouette: Boolean,
            url: String,
            asid: String,
            width: String
        },
        fbPages: [
            {
                access_token:String,
                category: String,
                category_list: [
                    {
                        id: String,
                        name: String
                    }
                ],
                name: String,
                id: String,
                tasks: [{type:String}]
            }
        ],
        password: String,
        createdAt: { type: Date, default: Date.now },
        updatedAt:{ type: Date, default: Date.now }
    }
});

module.exports = mongoose.model('user', UserSchema, 'users'); 

