const mongoose = require('mongoose');

const IntroPage = mongoose.Schema({
    message: String,
    data: {
	intro:{
    	firstName : String,
	b1 : String,
	b2 : String,
	b3 : String,
	b4 : String,
	b5 : String,
	feature : String
	}
}});

module.exports = mongoose.model('intro', IntroPage, 'users');
