'use strict';

const { createLogger, format, transports } = require('winston');
const fs = require('fs');
const path = require('path');
const { isObject } = require('lodash');
const env = process.env.NODE_ENV || 'development';
const logDir = 'log';

// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

const filename = path.join(logDir, 'logs1.log');
function formatObject(param) {
  if (isObject(param)) {
    return JSON.stringify(param);
  }
  return param;
}
const logger = caller => {
  return createLogger({
    // change level if in dev environment versus production
    level: env === 'production' ? 'info' : 'debug',
    format: format.combine(
      format.label({ label: path.basename(caller) }),
      format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' })
    ),
    transports: [
      new transports.Console({

        format: format.combine(
          format.colorize(),
          format.printf(
            info =>
              `${info.timestamp} ${info.level} [${info.label}]: ${formatObject(info.message)} `
          )
        )
      }),
      new transports.File({
        filename,
        format: format.combine(
          format.printf(
            info =>
              `${info.timestamp} ${info.level} [${info.label}]: ${formatObject(info.message)}`
          )
        )
      })
    ]
  });
};

module.exports = logger;