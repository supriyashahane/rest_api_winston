module.exports = function(app) {
	const logger = require('../logger/logger.js')(__filename);

	var express = require("express");
	var router = express.Router();
	
    const users = require('../controllers/user.controller.js');
    const users1 = require('../controllers/user.controller.js');

	router.use(function (req,res,next) {
		console.log("/" + req.method);
		next();
	});

	app.all("*", (req, res, next)=>{
		logger.info("incoming req");
		logger.info(req.method, "incoming req");
		//use debug when u need to dump out everything u need
		/*logger.debug("Incoming request verbose", {
			headers: req.headers,
			query: req.query,
			body: req.body
		});*/
		return next();
	})
	
    // Save a User to MongoDB
	app.post('/api/user/saveuser', users.saveUser);
	
	app.post('/api/intro/saveuser', users1.introSave);
 
    // Retrieve all Users
	app.get('/api/user/allusers', users.findUsers);
	app.get('/api/intro/allusers1', users1.findUsers1);
	
	
	app.use("/",router);
 
}